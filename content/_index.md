## Second Brain by Example

A Second Brain is a system of personal knowledge management that was popularized and created by Tiago Forte. Its a system that enables an individual to fully cultivate their knowledge.

- CODE 
- Collect
- Organize
- Distill
- Express
- PARA 
- Projects
- Areas
- Resources
- Archive
- Funny Examples
- Markdown
- Asciidoc
- Obsidian
-

<!-- This website is powered by the [Hugo](https://gohugo.io/) static site generator and [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/). This site is inspired by the following sites: [go by example](https://gobyexample.com/), [flutter by example](https://flutterbyexample.com/), [dart by example](https://www.jpryan.me/dartbyexample/), [haskell by example](https://lotz84.github.io/haskellbyexample/) and other sites of a similar naming structure. -->