<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Under what circumstances should we step off a path? When is it essential that we finish what we start? If I bought a bag of peanuts and had an allergic reaction, no one would fault me if I threw it out. If I ended a relationship with a woman who hit me, no one would say that I had a commitment problem. But if I walk away from a seemingly secure route because my soul has other ideas, I am a flake?">
<meta name="theme-color" content="#FFFFFF">
<meta name="color-scheme" content="light dark"><meta property="og:title" content="Flake it till you make it" />
<meta property="og:description" content="Under what circumstances should we step off a path? When is it essential that we finish what we start? If I bought a bag of peanuts and had an allergic reaction, no one would fault me if I threw it out. If I ended a relationship with a woman who hit me, no one would say that I had a commitment problem. But if I walk away from a seemingly secure route because my soul has other ideas, I am a flake?" />
<meta property="og:type" content="article" />
<meta property="og:url" content="https://www.example.com/post/2015-02-26-flake-it-till-you-make-it/" /><meta property="article:section" content="post" />
<meta property="article:published_time" content="2015-02-26T00:00:00+00:00" />
<meta property="article:modified_time" content="2019-11-13T23:23:21+01:00" />

<title>Flake it till you make it | 2nd Brain by Example</title>
<link rel="manifest" href="/manifest.json">
<link rel="icon" href="/favicon.png" type="image/x-icon">
<link rel="stylesheet" href="/book.min.ab46de3e725a6415339a37bba23a0067534a37289b063c9f8d011515a63097a8.css" integrity="sha256-q0bePnJaZBUzmje7ojoAZ1NKNyibBjyfjQEVFaYwl6g=" crossorigin="anonymous">
  <script defer src="/flexsearch.min.js"></script>
  <script defer src="/en.search.min.54dc416c1e3292a27c2073d99c946991a68b1bb7c93357d9245b250ade46d143.js" integrity="sha256-VNxBbB4ykqJ8IHPZnJRpkaaLG7fJM1fZJFslCt5G0UM=" crossorigin="anonymous"></script>

  <script defer src="/sw.min.6f6f90fcb8eb1c49ec389838e6b801d0de19430b8e516902f8d75c3c8bd98739.js" integrity="sha256-b2&#43;Q/LjrHEnsOJg45rgB0N4ZQwuOUWkC&#43;NdcPIvZhzk=" crossorigin="anonymous"></script>
<!--
Made with Book Theme
https://github.com/alex-shpak/hugo-book
-->
  
</head>
<body dir="ltr">
  <input type="checkbox" class="hidden toggle" id="menu-control" />
  <input type="checkbox" class="hidden toggle" id="toc-control" />
  <main class="container flex">
    <aside class="book-menu">
      <div class="book-menu-content">
        
  <nav>
<h2 class="book-brand">
  <a class="flex align-center" href="/"><span>2nd Brain by Example</span>
  </a>
</h2>


<div class="book-search">
  <input type="text" id="book-search-input" placeholder="Search" aria-label="Search" maxlength="64" data-hotkeys="s/" />
  <div class="book-search-spinner hidden"></div>
  <ul id="book-search-results"></ul>
</div>



  



  
    
  



<ul class="book-languages">
  <li>
    <input type="checkbox" id="languages" class="toggle" />
    <label for="languages" class="flex justify-between">
      <a role="button" class="flex align-center">
        <img src="/svg/translate.svg" class="book-icon" alt="Languages" />
        English
      </a>
    </label>

    <ul>
      
      <li>
        <a href="https://www.example.com/ru/">
          Chinese
        </a>
      </li>
      
    </ul>
  </li>
</ul>











  












  
<ul>
  
  <li>
    <a href="https://github.com/alex-shpak/hugo-book" target="_blank" rel="noopener">
        Github
      </a>
  </li>
  
  <li>
    <a href="https://themes.gohugo.io/hugo-book/" target="_blank" rel="noopener">
        Hugo Themes
      </a>
  </li>
  
</ul>






</nav>




  <script>(function(){var a=document.querySelector("aside .book-menu-content");addEventListener("beforeunload",function(b){localStorage.setItem("menu.scrollTop",a.scrollTop)}),a.scrollTop=localStorage.getItem("menu.scrollTop")})()</script>


 
      </div>
    </aside>

    <div class="book-page">
      <header class="book-header">
        
  <div class="flex align-center justify-between">
  <label for="menu-control">
    <img src="/svg/menu.svg" class="book-icon" alt="Menu" />
  </label>

  <strong>Flake it till you make it</strong>

  <label for="toc-control">
    
    <img src="/svg/toc.svg" class="book-icon" alt="Table of Contents" />
    
  </label>
</div>


  
  <aside class="hidden clearfix">
    
  
<nav id="TableOfContents"></nav>



  </aside>
  
 
      </header>

      
      
  <article class="markdown"><p>Under what circumstances should we step off a path? When is it essential that we finish what we start? If I bought a bag of peanuts and had an allergic reaction, no one would fault me if I threw it out. If I ended a relationship with a woman who hit me, no one would say that I had a commitment problem. But if I walk away from a seemingly secure route because my soul has other ideas, I am a flake?</p>
<p>The truth is that no one else can definitively know the path we are here to walk. It’s tempting to listen—many of us long for the omnipotent other—but unless they are genuine psychic intuitives, they can’t know. All others can know is their own truth, and if they’ve actually done the work to excavate it, they will have the good sense to know that they cannot genuinely know anyone else’s. Only soul knows the path it is here to walk. Since you are the only one living in your temple, only you can know its scriptures and interpretive structure.</p>
<p>At the heart of the struggle are two very different ideas of success—survival-driven and soul-driven. For survivalists, success is security, pragmatism, power over others. Success is the absence of material suffering, the nourishing of the soul be damned. It is an odd and ironic thing that most of the material power in our world often resides in the hands of younger souls. Still working in the egoic and material realms, they love the sensations of power and focus most of their energy on accumulation. Older souls tend not to be as materially driven. They have already played the worldly game in previous lives and they search for more subtle shades of meaning in this one—authentication rather than accumulation. They are often ignored by the culture at large, although they really are the truest warriors.</p>
<p>A soulful notion of success rests on the actualization of our innate image. Success is simply the completion of a soul step, however unsightly it may be. We have finished what we started when the lesson is learned. What a fear-based culture calls a wonderful opportunity may be fruitless and misguided for the soul. Staying in a passionless relationship may satisfy our need for comfort, but it may stifle the soul. Becoming a famous lawyer is only worthwhile if the soul demands it. It is an essential failure if you are called to be a monastic this time around. If you need to explore and abandon ten careers in order to stretch your soul toward its innate image, then so be it. Flake it till you make it.</p>
</article>
 
      

      <footer class="book-footer">
        
  <div class="flex flex-wrap justify-between">


  <div><a class="flex align-center" href="https://github.com/alex-shpak/hugo-book/commit/b6a119771a69c8103a2b81d7cc5d1d3cb2f38dc6" title='Last modified by GitLab | November 13, 2019' target="_blank" rel="noopener">
      <img src="/svg/calendar.svg" class="book-icon" alt="Calendar" />
      <span>November 13, 2019</span>
    </a>
  </div>



  <div>
    <a class="flex align-center" href="https://github.com/alex-shpak/hugo-book/edit/main/exampleSite/content/post/2015-02-26-flake-it-till-you-make-it.md" target="_blank" rel="noopener">
      <img src="/svg/edit.svg" class="book-icon" alt="Edit" />
      <span>Edit this page</span>
    </a>
  </div>


</div>



  <script>(function(){function a(c){const a=window.getSelection(),b=document.createRange();b.selectNodeContents(c),a.removeAllRanges(),a.addRange(b)}document.querySelectorAll("pre code").forEach(b=>{b.addEventListener("click",function(c){a(b.parentElement),navigator.clipboard&&navigator.clipboard.writeText(b.parentElement.textContent)})})})()</script>


 
        
      </footer>

      
  
  <div class="book-comments">

</div>
  
 

      <label for="menu-control" class="hidden book-menu-overlay"></label>
    </div>

    
    <aside class="book-toc">
      <div class="book-toc-content">
        
  
<nav id="TableOfContents"></nav>


 
      </div>
    </aside>
    
  </main>

  
</body>
</html>












